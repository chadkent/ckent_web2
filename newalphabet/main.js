//Made by Chad Kent | 10.22.18
//Program converts normal alphabet to special alphabet

void function newalphabet()
{
    const readline = require('readline-sync');

    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    var assert = require('assert');

    //input
    var message = rl.question('');
    message = message.toLowerCase();
    var len = message.length;
    message = message.split('');
    //conversion
    var i;
    var convert;
    for (i = 0; i > len; i++)
    {
        if (message[i] == 'a'){
            convert += "@";
        } else if (message[i] == 'b') {
            convert += "8";
        } else if (message[i] == 'c') {
            convert += "(";
        } else if (message[i] == 'd') {
            convert += "|)";
        } else if (message[i] == 'e') {
            convert += "3";
        } else if (message[i] == 'f') {
            convert += "#";
        } else if (message[i] == 'g') {
            convert += "6";
        } else if (message[i] == 'h') {
            convert += "[-]";
        } else if (message[i] == 'i') {
            convert += "|";
        } else if (message[i] == 'j') {
            convert += "_|";
        } else if (message[i] == 'k') {
            convert += "|<";
        } else if (message[i] == 'l') {
            convert += "1";
        } else if (message[i] == 'm') {
            convert += "[]\\/[]";
        } else if (message[i] == 'n') {
            convert += "[]\\[]";
        } else if (message[i] == 'o') {
            convert += "0";
        } else if (message[i] == 'p') {
            convert += "|D";
        } else if (message[i] == 'q') {
            convert += "(,)";
        } else if (message[i] == 'r') {
            convert += "|Z";
        } else if (message[i] == 's') {
            convert += "$";
        } else if (message[i] == 't') {
            convert += "']['";
        } else if (message[i] == 'u') {
            convert += "|_|";
        } else if (message[i] == 'v') {
            convert += "\\/";
        } else if (message[i] == 'w') {
            convert += "\\/\\/";
        } else if (message[i] == 'x') {
            convert += "}{";
        } else if (message[i] == 'y') {
            convert += "`/";
        } else if (message[i] == 'z') {
            convert += "2";
        } else {
            convert += message[i];
        }
    }
    //output
    console.log(convert);
}

assert(newalphabet("Hello World!" == "[-]3110 \\/\\/0|Z1|)!"));