//Made by Chad Kent | 10.04.18
//Program asks user to play a number guessing game.
//Will output statistics and responses for too low and too high guesses 
const readline = require('readline-sync');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var name = rl.question("Hi, what's your name?");

console.log("Hello " + name + ", lets play the number guessing game");

var games = 0;
var timesWon = 0;
var timesLost = 0;
var fastest = 10;

var play = 'y'
while (play == 'y') {

    var num = Math.floor(Math.random() * 20) + 1;
    var guess = 0;
    var tries = 1;
    var won = 0;

    for (tries = 1; tries < 7; tries++) {
        guess = rl.question("Guess a number: ");
   
        if (guess < num) {
            console.log("Too high");
        } else if (guess > num) {
            console.log("Too low");
        } else {
            console.log("You go it")
            won++;
            break;
        }
    }

    if (won == 1) {
        console.log("You solved it in " + tries + " step(s)");
        if (tries < fastest) {
            fastest = tries;
        }
        timesWon++;
    } else {
        console.log("You lost, the number was " + num);
        timesLost++;
    }
    games++;
    play = rl.question("Would you like to play again? Enter [y/n]: ");
}

console.log("You played " + games + " time(s)");
console.log("You won " + timesWon + " time(s)");
console.log("You lost " + timesLost + " time(s)");
console.log("Fastest guess: " + fastest);
