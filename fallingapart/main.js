// Chad Kent | 10.31.18
// Divides an array of integers into 2 optimal piles

void function fallingapart()
{
    var assert = require('assert');

    const readline = require('readline-sync');

    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    //read amount
    var amount;
    var pieces;
    rl.on('line', (amount) => {
        pieces = parseInt(amount);
    });

    //read values
    var nums;
    rl.on('line', (nums) => {
        //split into array of strings
        nums = nums.split(' ');
    });
    var num = [pieces];
    //copy values into array of ints
    for (var i = 0; i < pieces; i++)
    {
        num[i] = parseInt(nums[i]);
    }
    var alice;
    var bob;
    //divide array
    for (var i = 0; i < pieces; i++)
    {
        var temp = 0;
        var index = 0;
        //find highest value
        for (var j = 0; j < pieces; j++)
        {
            if (temp < num[j])
            {
                temp = num[j];
                index = j;
            }
        }
        //add highest value to pile
        alice += temp;
        //remove value from array
        num[index] = 0;
        temp = 0;
        
        //find highest value
        for (var j = 0; j < pieces; j++)
        {
            if (temp < num[j])
            {
                temp = num[j];
                index = j;
            }
        }
        //add highest value to pile
        bob += temp;
        //remove value from array
        num[index] = 0;
        temp = 0;
    }
    //print answer
    console.log(alice + " " + bob);
}

assert(fallingapart("5", "4 5 2 7 1") = "12 7")
assert(fallingapart("17", "8 8 9 2 2 2 3 4 2 6 8 5 9 5 4 4 3") = "45 39")